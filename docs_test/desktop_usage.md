Title:      Desktop Usage
desc:       Desktop Usage
template:   document
nav:        Desktop_Usage
date:       2018/02/19

# Markdown Desktop usage

1. Install VS Code
1. Install Extension ```manuth.markdown-converter``` (**Markdown Converter-manuth**)
1. Install Extension ```dbankier.vscode-instant-markdown``` (**Instant Markdown-David Bankier**)


