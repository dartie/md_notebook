from flask import Flask, render_template, request, redirect, url_for, send_from_directory
from custom import print_Network_info  # getIpAddresses, getHostname, getExtIPAddress
from cartello.cartello import process_markdown as cartello_process_markdown
import datetime
import sys
import os
import io
import shutil
from werkzeug.routing import BaseConverter
import threading
from http.server import HTTPServer, SimpleHTTPRequestHandler
from search_in_html.search_in_html import get_occurrences as search_html_get_occurrences

sys.path.append("..")
from directory_tree_to_html import tree2html_tree_view  # list_files_html

# import Namespace
try:
    from types import SimpleNamespace as Namespace  # available from Python 3.3
except ImportError:
    class Namespace:
        def __init__(self, **kwargs):
            self.__dict__.update(kwargs)

app = Flask(__name__, static_url_path='/static')
# app.config['SEND_FILE_MAX_AGE_DEFAULT'] = 0

# custom routing
class RegexConverter(BaseConverter):
    def __init__(self, url_map, *items):
        super(RegexConverter, self).__init__(url_map)
        self.regex = items[0]


app.url_map.converters['regex'] = RegexConverter
#

# global vars
flaskapp_port = 5001
serverlocalfiles_port = 5002
docnado_port = 5000
docnado_webpage = 'http://0.0.0.0:{port}'.format(port=docnado_port)

docs_root = 'docs'
boards_root = os.path.join(docs_root, 'Boards').replace('\\', '/')
docs_root = os.path.realpath(docs_root)


def run_docnado(port=5000):
    os.system('docnado')


def serve_local_docs(port=8000, directory=os.getcwd()):
    class Handler(SimpleHTTPRequestHandler):
        def __init__(self, *args, **kwargs):
            super().__init__(*args, directory=directory, **kwargs)

    httpd = HTTPServer(('localhost', port), Handler)
    httpd.serve_forever()


def get_last_modified_date_time_utc(f, format='%Y-%m-%d %H:%M:%S'):
    modTimesinceEpoc = os.path.getmtime(f)
    modificationTime = datetime.datetime.utcfromtimestamp(modTimesinceEpoc).strftime(format)

    return modificationTime


def remove_suffix(text, suffix):
    if text.endswith(suffix):
        return text[:len(text) - len(suffix)]
    return text  # or whatever


def add_docnado_metatag_to_markdown(f):
    # check if already present
    read_stream = io.open(f, 'r', encoding='utf-8', errors='ignore')
    firstline = read_stream.readline()
    read_stream.close()
    if firstline.strip().startswith('Title'):
        return  # the header has been already added in past

    # set Title, Description, Navigation path and last update info
    Title = os.path.basename(os.path.splitext(f)[0]).title().replace('_', ' ')
    Description = Title

    rel_path = os.path.relpath(f, os.getcwd())
    rel_path_list = rel_path.split(os.sep)[1:]
    rel_path_list = [remove_suffix(x, '.md').title() for x in rel_path_list]
    Path = '>'.join(rel_path_list)

    Last_update = get_last_modified_date_time_utc(f, '%Y/%m/%d')

    # add header
    header = """Title:      {Title}
desc:       {Description}
template:   document
nav:        {Path}
date:       {Last_update}

""".format(Title=Title, Description=Description, Path=Path, Last_update=Last_update)

    with io.open(f, 'r', encoding='utf-8', errors='ignore') as read_fullfile:
        md_content = read_fullfile.read()
    new_md_content = header + md_content

    # write new markdown file for docnado
    with io.open(f, 'w', encoding='utf-8', errors='ignore') as write_md:
        write_md.write(new_md_content)


def add_docnado_metatag(docs_root, kanban_boards_folder='boards'):
    for scanpath, folders, files in os.walk(docs_root):

        if scanpath.lower() == kanban_boards_folder:
            continue  # skip boards folder, because it's reserved to kanban boards documents

        for f in [x for x in files if x.endswith('.md')]:
            fullfilepath = os.path.join(scanpath, f)
            add_docnado_metatag_to_markdown(fullfilepath)


def render_board(file, fullpathfile):
    f_board_title = os.path.splitext(os.path.basename(fullpathfile))[0]
    board_html = cartello_process_markdown(fullpathfile, f_board_title.title())

    # inject css content
    cartello_css_file = os.path.join('cartello', 'site', 'bulma.min.css')

    with io.open(cartello_css_file, 'r', encoding='utf-8', errors='ignore') as read_cartello_css:
        cartello_css_content = read_cartello_css.read()

        cartello_css_injection = """<style type="text/css">
    %s
    </style>
    """ % cartello_css_content
    board_html = board_html.replace('<link rel="stylesheet" href="./bulma.min.css">', cartello_css_injection)

    f_board_htmlfile = os.path.join(os.path.dirname(fullpathfile), f_board_title + '.html')
    with io.open(f_board_htmlfile, 'w', encoding='utf-8', errors='ignore') as write_html_board:
        write_html_board.write(board_html)

    board_tree = get_sidebar_tree(boards_root, False, True, r'\.md$')

    return render_template('index.html', docnado_webpage=docnado_webpage, cartello_html='http://0.0.0.0:{}/'.format(serverlocalfiles_port) + os.path.splitext(file)[0] + '.html', board_tree=board_tree)


def render_doc(file, fullpathfile):
    return render_template('index.html', docnado_webpage=docnado_webpage + '/w/' + file)


def get_sidebar_tree(startpath, include_root=True, link_files=False, reg_ext_filter=False):
    tree_snippet = tree2html_tree_view.list_files_html(startpath, include_root, link_files, reg_ext_filter)
    tree_snippet_beginning, tree_snippet_end, html_beginning, html_end = tree2html_tree_view.html_fix_part()
    board_tree = tree_snippet_beginning + tree_snippet + tree_snippet_end
    board_tree = os.linesep.join(board_tree)
    # add margin
    board_tree = board_tree.replace('id="myUL"', 'id="myUL" style="margin:10px"')

    return board_tree


@app.route('/search', methods=['GET', 'POST'])
def search():
    criteria = request.form.get("criteria")
    regex_flag = request.form.get("regex_flag")
    ignorecase_flag = request.form.get("ignorecase_flag")

    search_results_dict = {}
    for r, d, files in os.walk(docs_root):
        for f in files:
            fullfilepath = os.path.join(r, f)
            search_results_dict[fullfilepath] = search_html_get_occurrences(criteria, fullfilepath, regex_flag, ignorecase_flag)
    board_tree = get_sidebar_tree(boards_root, False, True, r'\.md$')

    options = []
    if regex_flag:
        options.append("regex")
    if ignorecase_flag:
        options.append("ignorecase")
    options = ' - '.join(options)

    return render_template('index.html', docnado_webpage=docnado_webpage, cartello_html='http://0.0.0.0:5002/Boards/todo.html', board_tree=board_tree, search_results=search_results_dict, search_criteria=criteria, options=options)


@app.route('/')
def home():
    board_tree = get_sidebar_tree(boards_root, False, True, r'\.md$')

    return render_template('index.html', docnado_webpage=docnado_webpage, cartello_html='http://0.0.0.0:5002/Boards/todo.html', board_tree=board_tree)


@app.route('/<regex(".+"):file>')
def route_doc_links(file):
    """
    :param file: possible inputs: 1) .md for docnado ; 2) .md for board (/docs/Boards)
    :return:
    """
    fullpathfile = os.path.join('docs', file)
    fullpathfile = os.path.realpath(fullpathfile)
    if not os.path.exists(fullpathfile):
        return 'The file does not exists'  # TODO: return the home page for board

    if not fullpathfile.lower().endswith('.md'):
        return 'The file is not markdown'  # TODO: return the home page for board

    # get the document type (docnado or cartello (board) )
    board_file = False
    doc_file = False
    filepath_splitted = file.split('/')
    if len(filepath_splitted) > 1:
        if filepath_splitted[0] == 'Boards':
            board_file = True
        else:
            doc_file = True
    else:
        doc_file = True

    # rendere the correct page
    if board_file:
        return render_board(file, fullpathfile)
    elif doc_file:
        return render_doc(file, fullpathfile)


if __name__ == '__main__':
    # I obtain the app directory
    if getattr(sys, 'frozen', False):
        # frozen
        dirapp = os.path.dirname(sys.executable)
        dirapp_bundle = sys._MEIPASS
        executable_name = os.path.basename(sys.executable)
    else:
        # unfrozen
        dirapp = os.path.dirname(os.path.realpath(__file__))
        dirapp_bundle = dirapp
        executable_name = os.path.basename(__file__)

    # create home.md for docnado in case it does not exist
    home_md_filename = os.path.join(docs_root, 'home.md')
    if not os.path.exists(home_md_filename):
        with open(home_md_filename, 'w') as write_homemd:
            write_homemd.write('# Home')
    # run docnado flask server
    t1 = threading.Thread(target=run_docnado, args=[])
    # t1.start()

    # run server for serving local html files which change dynamically (html boards from cartello module)
    t2 = threading.Thread(target=serve_local_docs, args=[serverlocalfiles_port, docs_root])
    t2.start()
    # print(t2.getName())

    add_docnado_metatag(docs_root)

    app.run(host="0.0.0.0", port=flaskapp_port, debug=True)


# TODO : custom style docnado (if file found in the current directory, copy it to docnado before running it)
# TODO : search
# TODO : edit
# TODO : add TOC to documents

# TODO: python os.path get file regardless the case

# TODO: docnado needs to be run manually for avoiding confusion
# TODO: icons in navbar
# TODO: save docnado as html
"""
changes made in "cartello" module

* strippedNames argument in "process_markdown" function, must have a default value () process_markdown(fp, current, strippedNames=())
* removed tag <div class="hero-foot">

"""

# * https://stackoverflow.com/questions/34901523/file-url-not-allowed-to-load-local-resource-in-the-internet-browser/42294756
