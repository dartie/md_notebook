function sidebar_toggle(sidebar_id, main_id, sidebar_width="160px"){
    var sidebar = document.getElementById(sidebar_id);
    var main = document.getElementById(main_id);
    //var sidebar = document.getElementsByClassName(sidebar_id);
    
    if (sidebar.style.width === "0px"){
        sidebar.style.width = sidebar_width;
        main.style.marginLeft = sidebar_width;
    }
    else{
        sidebar.style.width = "0px";
        main.style.marginLeft = "0px";
    }
}

function sidebar_toggle_(sidebar_id, main_id, main_margin="160px"){
    var sidebar = document.getElementById(sidebar_id);
    var main = document.getElementById(main_id);
    //var sidebar = document.getElementsByClassName(sidebar_id);
    
    if (sidebar.style.display === "block"){
        sidebar.style.display = "none";
        main.style.marginLeft = "0px";
    }
    else{
        sidebar.style.display = "block";
        main.style.marginLeft = main_margin;
    }
}
