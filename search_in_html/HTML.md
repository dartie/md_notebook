# HTML
## Basic elements
### Button
```html
<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
.button {
    background-color: #4CAF50; /* Green */
    border: none;
    color: white;
    padding: 20px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 16px;
    margin: 4px 2px;
    cursor: pointer;
}

.button1 {border-radius: 2px;}
.button2 {border-radius: 4px;}
.button3 {border-radius: 8px;}
.button4 {border-radius: 12px;}
.button5 {border-radius: 50%;}
</style>
</head>
<body>

<h2>Rounded Buttons</h2>
<p>Add rounded corners to a button with the border-radius property:</p>

<button class="button button1">2px</button>
<button class="button button2">4px</button>
<button class="button button3">8px</button>
<button class="button button4">12px</button>
<button class="button button5">50%</button>

</body>
</html>
```

### Checkbox
```html
<input id="checkBox" type="checkbox">
```

#### Text is clickable
```html
  <div>
    <input type="checkbox" id="coding" name="interest" value="coding" checked>
    <label for="coding">Coding</label>
  </div>
```


### searchbox
```css
#namanyay-search-btn {
background:#0099ff;
color:white;
font: 'trebuchet ms', trebuchet;
padding:10px 20px;
border-radius:0 10px 10px 0;
-moz-border-radius:0 10px 10px 0;
-webkit-border-radius:0 10px 10px 0;
-o-border-radius:0 10px 10px 0;
border:0 none;
font-weight:bold;
}
 
#namanyay-search-box {
background: #eee;
padding:10px;
 border-radius:10px 0 0 10px;
-moz-border-radius:10px 0 0 10px;
-webkit-border-radius:10px 0 0 10px;
-o-border-radius:10px 0 0 10px;
border:0 none;
width:160px;
 }
```

```html
<form id="searchthis" action="/search" style="display:inline;" method="get">
  <!-- Search box for blogger by Namanyay Goel //-->
  <input id="namanyay-search-box" name="q" size="40" type="text" placeholder="  Type! :D "/>
  <input id="namanyay-search-btn" value="Search" type="submit"/>
</form>
```



## Turn off Form Autocompletion
### username
```html
<input autocomplete="nope">
```

### password
```html
<input autocomplete="new-password">
```

### All the rest
```html
<input autocomplete="off">
```

### Reference
* https://developer.mozilla.org/en-US/docs/Web/Security/Securing_your_site/Turning_off_form_autocompletion

## Disable text selection from an element
```css
.noselect {
  -webkit-touch-callout: none; /* iOS Safari */
    -webkit-user-select: none; /* Safari */
     -khtml-user-select: none; /* Konqueror HTML */
       -moz-user-select: none; /* Firefox */
        -ms-user-select: none; /* Internet Explorer/Edge */
            user-select: none; /* Non-prefixed version, currently
                                  supported by Chrome and Opera */
}
```


## Sidebar
**Reference**
* [w3 Notes](https://www.w3schools.com/w3css/w3css_sidebar.asp)
* [Fixed Sidebar](https://www.w3schools.com/howto/howto_css_fixed_sidebar.asp)

### Html
```html
<div class="top">
    TOP
</div>
<div class="left">
    LEFT
</div>
<div class="main">
    MAIN
</div>
```

### css
```css
.top {
    position:absolute;
    left:0; right:0;
    height: 92px;
}
.left {
    position:absolute;
    left:0; top:92px; bottom: 0;
    width: 178px;
}
.main {
    position: absolute;
    left:178px; top:92px; right:0; bottom:0;
}

.top { background: blue; }
.left { background: red; }
.main { background: yellow; }
```

## Link

### Different URL Forms

* https://html.com/attributes/a-href/

The URL may be:

Fully Qualified (include a protocol)

`https://html.com`

`https://html.com`

URL with a relative (unspecified) protocol

`//html.com`

Browser-specific protocol

`chrome://settings/`

Relative to the current page

`next`

Relative to the current domain

`/`

`/wp-content/uploads/flamingo.jpg`

## Values of the href Attribute

Value Name

Notes

url

The URL (URI) of the linked resource.

## All Attributes of the  [`anchor`](https://html.com/tags/a/)  Element

  
  
Read more:  [https://html.com/attributes/a-href/#ixzz5p4NZTxJ3](https://html.com/attributes/a-href/#ixzz5p4NZTxJ3)
<!--stackedit_data:
eyJoaXN0b3J5IjpbLTE1Nzk4NDk4MzIsOTg2OTU4MjI3LC04Nz
E4MzI1NjYsNDc0ODkzMjE4LDg2NDI1NTQ1NiwtOTM3Njc5MTg2
XX0=
-->