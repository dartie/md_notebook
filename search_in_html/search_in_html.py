import re, io
from bs4 import BeautifulSoup
import markdown2 as markdown


def get_occurrences_from_html_text(html_file, text, regexFlag, ignorecaseFlag):
    #soup = BeautifulSoup(html_file, features="lxml")
    soup = BeautifulSoup(html_file, "html.parser")

    page_md = soup.findAll()

    tag_text = []
    [tag_text.append(x.text) for x in page_md]
    tag_text = '\n'.join(tag_text)

    if regexFlag:
        regex_flags = []
        if ignorecaseFlag:
            regex_search = re.compile(text, re.IGNORECASE)
        else:
            regex_search = re.compile(text)
        occurrences = len(re.findall(regex_search, tag_text))
    else:
        if ignorecaseFlag:
            occurrences = tag_text.lower().count(text.lower())
        else:
            occurrences = tag_text.count(text)

    return occurrences


def convert_md_html(md_text):
    html = markdown.markdown(md_text, extras=["footnotes", "fenced-code-blocks"])

    return html


def get_occurrences(word, md_file, regexFlag, ignorecaseFlag):
    # convert markdown to html
    doc_html = convert_md_html(io.open(md_file, "r", encoding='utf-8', errors='ignore').read())
    with open('html.html', 'w') as write_html:
        write_html.write(doc_html)

    return get_occurrences_from_html_text(doc_html, word, regexFlag, ignorecaseFlag)


if __name__ == '__main__':
    # TEST
    filelist = ['HTML.md']
    occurrences_dict = {}  # {file : occurrences}
    for f in filelist:
        occ = get_occurrences('button', f, True, True)
        occurrences_dict[f] = occ

    print(occurrences_dict)
