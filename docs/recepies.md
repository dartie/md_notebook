Title:      Recepies
desc:       Recepies
template:   document
nav:        Recepies
date:       2019/02/02


# Recepies

## Pizza
### Margherita con pasta acquistata 
**Necessario:**
- [ ] Pasta pizza (ca' bianca - fresca - verace - spessa)
- [ ] Passata di pomodoro
- [ ] Basilico
- [ ] Parmigiano grattugiato
- [ ] Provola
- [ ] 1 ruoto

1. Riscaldare il forno a 250 gradi
2. Stendere la pasta nel ruoto
3. Spalmare **N** cucchiai di passata di pomodoro
4. Spargere il parmigiano
5. Tagliare la provola a pezzettini e spargerla
6. Applicare le foglie di basilico
7. Infornare per 10/20 minuti (controllare la cottura alla base dopo 10 minuti)


## Dolci

### Tiramisu'
**Necessario:**
- [ ] 1/2 Kg panna
- [ ] 4 uova
- [ ] 4 cucchiai zucchero
- [ ] savoiardi
- [ ] caffe'
- [ ] cacao
- [ ] 1 contenitore (da utilizzare per posizione il dolce)
- [ ] 2 contenitori per montare (albume e panna)

1. Montare a neve gli albumi
2. Aggiungere i tuorli e lo zucchero (2 cucchiai)
3. Montare la panna
    > Pulire bene le fruste! (altrimenti la panna si fa una chiavica)
    
4. Aggiungere il cacao spolverandolo
5. Unisci i 2 montaggi
6. Aggiungi liquore a piacere (strega o rum)
7. Preparare il caffe' (x 10 persone) e versarlo in una zuppiera
8. Aggiungere i restanti 2 cucchiai di zucchero nel caffe' ancora bollente (altrimenti non si scioglie)
9. Aggiungere il latte finche' diventa marroncino
10. Ad uno ad uno, bagnare i savoiardi  nella zuppiera contenente il caffe' e posizionarli nel contenitore del tiramisu' al fine di formare uno strato
    > Il primo strato di savoiardi deve essere bagnato poco
    
11. Dopo ogni strato aggiungere uno strato composto dalla crema ottenuta al punto 5
12. Riempire il contenitore e completare con uno strato di crema
13. Spolverare con il cacao

<!--stackedit_data:
eyJoaXN0b3J5IjpbNDM0MTM5MTcyXX0=
-->