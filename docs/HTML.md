Title:      Html
desc:       Html
template:   document
nav:        Html
date:       2018/05/22

# HTML
## Basic elements
### Button
```html
<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
.button {
    background-color: #4CAF50; /* Green */
    border: none;
    color: white;
    padding: 20px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 16px;
    margin: 4px 2px;
    cursor: pointer;
}

.button1 {border-radius: 2px;}
.button2 {border-radius: 4px;}
.button3 {border-radius: 8px;}
.button4 {border-radius: 12px;}
.button5 {border-radius: 50%;}
</style>
</head>
<body>

<h2>Rounded Buttons</h2>
<p>Add rounded corners to a button with the border-radius property:</p>

<button class="button button1">2px</button>
<button class="button button2">4px</button>
<button class="button button3">8px</button>
<button class="button button4">12px</button>
<button class="button button5">50%</button>

</body>
</html>
```

### Checkbox
```html
<input id="checkBox" type="checkbox">
```

#### Text is clickable
```html
  <div>
    <input type="checkbox" id="coding" name="interest" value="coding" checked>
    <label for="coding">Coding</label>
  </div>
```




## Turn off Form Autocompletion
### username
```html
<input autocomplete="nope">
```

### password
```html
<input autocomplete="new-password">
```

### All the rest
```html
<input autocomplete="off">
```

### Reference
* https://developer.mozilla.org/en-US/docs/Web/Security/Securing_your_site/Turning_off_form_autocompletion

## Disable text selection from an element
```css
.noselect {
  -webkit-touch-callout: none; /* iOS Safari */
    -webkit-user-select: none; /* Safari */
     -khtml-user-select: none; /* Konqueror HTML */
       -moz-user-select: none; /* Firefox */
        -ms-user-select: none; /* Internet Explorer/Edge */
            user-select: none; /* Non-prefixed version, currently
                                  supported by Chrome and Opera */
}
```


## Sidebar
**Reference**
* [w3 Notes](https://www.w3schools.com/w3css/w3css_sidebar.asp)
* [Fixed Sidebar](https://www.w3schools.com/howto/howto_css_fixed_sidebar.asp)

### Html
```html
<div class="top">
    TOP
</div>
<div class="left">
    LEFT
</div>
<div class="main">
    MAIN
</div>
```

### css
```css
.top {
    position:absolute;
    left:0; right:0;
    height: 92px;
}
.left {
    position:absolute;
    left:0; top:92px; bottom: 0;
    width: 178px;
}
.main {
    position: absolute;
    left:178px; top:92px; right:0; bottom:0;
}

.top { background: blue; }
.left { background: red; }
.main { background: yellow; }
```
<!--stackedit_data:
eyJoaXN0b3J5IjpbLTg3MTgzMjU2Niw0NzQ4OTMyMTgsODY0Mj
U1NDU2LC05Mzc2NzkxODZdfQ==
-->