Title:      Html
desc:       Html
template:   document
nav:        Web_Dev>Html
date:       2019/02/22

# HTML

## Disable browser caching with meta HTML tags
* https://stackoverflow.com/questions/42855187/how-to-add-version-number-to-html-file-not-only-to-css-and-js-files
* http://cristian.sulea.net/blog/disable-browser-caching-with-meta-html-tags/

```python
resp.headers["Cache-Control"] = "no-cache, no-store, must-revalidate"
resp.headers["Pragma"] = "no-cache"
resp.headers["Expires"] = "0"
```
<!--stackedit_data:
eyJoaXN0b3J5IjpbOTE3NDU0MTM5LC0zOTAyNzc3MzJdfQ==
-->