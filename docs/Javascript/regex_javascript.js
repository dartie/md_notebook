/* Common variables */

var text = '[description:"aoeu" uuid:"123sth"]';
var regex = /\s*([^[:]+):\"([^"]+)"/g;


/* ---------------------- */


/* 1 */ // basic
var match;

do {
    match = regex.exec(text);
    if (match) {
        console.log(match[1]);
    }
} while (match);



/* 2 */  // single group
var match;
var matches_total = [];
do {
    match = regex.exec(text);
    if (match) {
        console.log(match[1]);
        matches_total.push(match[1]);
    }
} while (match);


/* 3 */  // all groups - does not need configuration
var match;
var matches_total = [];
do {
    match = regex.exec(text);
    if (match) {
        
        for(var i=0; i < match.length; i++){
            if(i==0){
                continue;  // skip the full match string
            }
            console.log(match[i]);
            matches_total.push(match[i]);
        }

    }
    
} while (match);

console.log(matches_total);

/* 4 */ // function, in order to work as in python
function regex_findall(regex, text){
 
    var match;
    var matches_total = [];
    do {
        match = regex.exec(text);
        if (match) {
            
            for(var i=0; i < match.length; i++){
                if(i==0){
                    continue;  // skip the full match string
                }
                matches_total.push(match[i]);
            }

        }
        
    } while (match);

    return matches_total;

}

all_matches_text = regex_findall(regex, text); // function call 
