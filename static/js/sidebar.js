function ToggleNav(mySidebar_id="mySidebar", main_id="main") {
  let sidebar = document.getElementById("mySidebar");
  let main = document.getElementById("main");

  if (sidebar.style.width == "0px" || sidebar.style.width == "0" || sidebar.style.width == ""){
	sidebar.style.width = "250px";
  	main.style.marginLeft = "250px";
  }
  else{
	sidebar.style.width = "0px";
  	main.style.marginLeft = "0px";
  }

}


function closeNav(mySidebar_id="mySidebar", main_id="main") {
    document.getElementById(mySidebar_id).style.width = "0";
    document.getElementById(main_id).style.marginLeft= "0";
}

function openNav(mySidebar_id="mySidebar", main_id="main") {
    /* pre-set */
    document.getElementById(mySidebar_id).style['-webkit-transition-duration'] = "0s";
    document.getElementById(main_id).style['-webkit-transition-duration'] = "0s";

    /* open nav */
    document.getElementById(mySidebar_id).style.width = "250px";
    document.getElementById(main_id).style.marginLeft = "250px";

    /* restore settings */
    document.getElementById(mySidebar_id).style['-webkit-transition-duration'] = "0.25s";
    document.getElementById(main_id).style['-webkit-transition-duration'] = "0.25s";
}
